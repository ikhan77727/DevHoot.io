---
layout: post
title: "Jekyll vs WordPress, And Why I Chose Jekyll!"
date: 2017-07-29 13:24:49
image: 'https://res.cloudinary.com/dm7h7e8xj/image/upload/c_scale,w_760/v1501345962/wordpress-to-jekyll_mx4ddm.png'
description:   Usually, most people end up choosing WordPress, mostly because they’ve heard of it, or know someone who’s using it.
category: 'blog'
tags:
- jekyll
- wordpress
- blog
twitter_text: Jekyll vs WordPress, And Why I Chose Jekyll!
introduction:  Usually, most people end up choosing WordPress, mostly because they’ve heard of it, or know someone who’s using it. 
---

<div class="post-actual-content">
                        <h3 id="introduction">Introduction</h3>

<p>I assure you, when you decide to have a blog of your own, you’re going to spend days, if not weeks, choosing the “perfect” blogging platform. Usually, most people end up choosing WordPress, mostly because they’ve heard of it, or know someone who’s using it. Of course, those who’ve been on the Internet for long enough, would also choose it because it’s open-source and has a lively community. And then there are PHP programmers, who choose it because they think they’ll be able to make all the customizations they want.</p>

<h3 id="advantages-of-using-jekyll">Advantages of Using Jekyll</h3>

<p>I, however, chose to use Jekyll. It was a hard decision, and, I’m not even sure if it was right. But I’ll stick to it for one reason, and one reason alone: Jekyll is a static site generator. That means, I can host Jekyll blogs on any web server that can serve static pages. I don’t need any databases, interpreters or security updates. I don’t even have to worry about performance. Why? Because my blog now consists of only HTML pages, CSS, JavaScript, and images.</p>

<p>What’s more, Jekyll offers unlimited flexibility. I have complete control over the looks of every single page in my blog. Now, you could argue that that’s possible with WordPress too. But, with Jekyll, it’s far, far easier, and far less time-consuming.</p>

<p>Some of you programmers might be asking how comments would work on a static blog. Ever heard of Disqus? Well, I’m sure you have, becaue most blogs use it today. Embedding Disqus comments in a Jekyll blog is a walk in the park.</p>

<p>One last advantage I’d like to mention is that setting up Jekyll on your computer takes less than five minutes. If you don’t have Ruby installed already, you can install it using any package manager. For example, if your are using Ubuntu, you could simply type in the following first:</p>

<div><div id="highlighter_543774" class="syntaxhighlighter  bash"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="gutter"><div class="line number1 index0 alt2">1</div></td><td class="code"><div class="container"><div class="line number1 index0 alt2"><code class="bash functions">sudo</code> <code class="bash plain">apt-get </code><code class="bash functions">install</code> <code class="bash plain">ruby2.0 ruby2.0-dev ruby2.0-bundler</code></div></div></td></tr></tbody></table></div></div>

<p>Once Ruby is installed, installing Jekyll takes just one command.</p>

<div><div id="highlighter_768372" class="syntaxhighlighter  bash"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="gutter"><div class="line number1 index0 alt2">1</div></td><td class="code"><div class="container"><div class="line number1 index0 alt2"><code class="bash plain">gem2.0 </code><code class="bash functions">install</code> <code class="bash plain">jekyll</code></div></div></td></tr></tbody></table></div></div>

<h3 id="hosting-jekyll-blogs">Hosting Jekyll Blogs</h3>

<p>When it comes to hosting a Jekyll blog, you’ve a lot of options. I mean, every web server can host static HTML pages. You can use paid services such as Amazon S3, Heroku, Digital Ocean, and the list goes on and on. But, you could also choose services such as GitHub, Gitlab, Firebase or App Engine, and run your blog for free. In fact, GitHub has built-in support for Jekyll blogs, and you can start using it right now by creating a new GitHub account and repository. The same can be said about GitLab.</p>

<p>You might now be wondering, <em>“if Jekyll is really so good, why don’t I see anybody using it?”</em> You’d be surprised how many blogs are using it these days. I suggest you spend a few minutes taking a look at this <a href="https://talk.jekyllrb.com/t/showcase-sites-made-using-jekyll/18">showcase</a>, or this <a href="https://github.com/jekyll/jekyll/wiki/sites">wiki</a>.</p>

<h3 id="shortcomings">Shortcomings</h3>

<p>I think I’ve praised Jekyll enough. Let’s talk about some of its inadequacies now. First of all, you’ll have to use the command line while working with it. You need to know just a handful of commands though. For example, to create a new Jekyll blog, you would have to say something like this:</p>

<div><div id="highlighter_623940" class="syntaxhighlighter  bash"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="gutter"><div class="line number1 index0 alt2">1</div></td><td class="code"><div class="container"><div class="line number1 index0 alt2"><code class="bash plain">jekyll new BearsAreFluffy</code></div></div></td></tr></tbody></table></div></div>

<p>And to make it ready for publishing, you would have to type in the following:</p>

<div><div id="highlighter_159361" class="syntaxhighlighter  bash"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="gutter"><div class="line number1 index0 alt2">1</div></td><td class="code"><div class="container"><div class="line number1 index0 alt2"><code class="bash plain">jekyll build</code></div></div></td></tr></tbody></table></div></div>

<p>If that scares you, you should consider using <a href="https://jekyllrb.com/news/2016/08/24/jekyll-admin-initial-release/">Jekyll Admin</a>, which offers a nice GUI alternative for Jekyll’s command line tools. However, it was released just a few days ago. So, don’t expect too much from it.</p>

<p>In my opinion, the only thing that is holding Jekyll back is a severe lack of good themes. While hundreds of new WordPress themes are published every week, there might be less than a dozen good Jekyll themes in the wild right now, if that. Recently, Jekyll changed its theming system. So, I’m hoping more theme developers start supporting Jekyll.</p>

<p>Here’s what the default Jekyll theme looks like (it’s called Minima):</p>

<p><img src="/assets/block1/jekyll_default_theme.png" class="img-responsive center-block img-thumbnail" alt="Default Jekyll theme (Minima)"></p>

<p>Jekyll plugins too are quite scarce, although there are some great ones, such as <code class="highlighter-rouge">jekyll-seo-tag</code> and <code class="highlighter-rouge">jekyll-paginate</code>. Adding and using a Jekyll plugin can also be quite an involved process.</p>

<p>Lastly, Jekyll uses Ruby. That means, PHP programmers will have to learn a few things before they can start adding custom functionality to Jekyll blogs.</p>

<h3 id="conclusion">Conclusion</h3>

<p>Jekyll definitely has a lot of potential, and is becoming increasing popular, mainly among bloggers who are also developers. Once there are enough themes, it should see more widespread usage. Until then, if you don’t like to get your hands dirty using Liquid templates and markdown, I’d say stick with WordPress. But if you want complete control over your blog, go ahead with Jekyll.</p>

<p>Oh, and Jekyll has importers for almost every popular blogging platform. So, if you want to switch to Jekyll, it takes less than ten minutes.</p>

<h4 id="related-reading">Related Reading</h4>

<ul>
  <li><a href="/2016/08/create-jekyll-theme-material-design.html">How to Create a Material Design Jekyll Theme</a></li>
  <li><a href="/2018/02/netlify-vs-github-pages-vs-firebase.html">Netlify vs GitHub Pages vs Firebase Hosting: Which one is better?</a></li>
</ul>

                    </div>