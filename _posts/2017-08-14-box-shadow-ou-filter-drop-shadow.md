---
layout: post
title: "box-shadow"
date: 2017-08-14 22:55:45
image: 'https://res.cloudinary.com/dm7h7e8xj/image/upload/c_scale,w_760/v1502757949/o-sombra_xyw4wq.jpg'
description: Used in casting shadows (often called "Drop Shadows", like in Photoshop) from elements. Here is an example with the deepest possible browser support
category: 'css'
tags:
- css
- frontend
twitter_text: "box-shadow"
introduction: Used in casting shadows (often called "Drop Shadows", like in Photoshop) from elements. Here is an example with the deepest possible browser support.
---
<article id="post-13974">

    <h1 class="screen-reader">box-shadow</h1>

    <div class="article-content">

      <p>Used in casting shadows (often called "Drop Shadows", like in Photoshop) from elements. Here is an example with the deepest possible browser support:</p>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token selector"><span class="token class">.shadow</span> </span><span class="token punctuation">{</span>
  <span class="token property">-webkit-box-shadow</span><span class="token punctuation">:</span> <span class="token number">3</span>px <span class="token number">3</span>px <span class="token number">5</span>px <span class="token number">6</span>px <span class="token hexcode">#ccc</span><span class="token punctuation">;</span>  <span class="token comment" spellcheck="true">/* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */</span>
  <span class="token property">-moz-box-shadow</span><span class="token punctuation">:</span>    <span class="token number">3</span>px <span class="token number">3</span>px <span class="token number">5</span>px <span class="token number">6</span>px <span class="token hexcode">#ccc</span><span class="token punctuation">;</span>  <span class="token comment" spellcheck="true">/* Firefox 3.5 - 3.6 */</span>
  <span class="token property">box-shadow</span><span class="token punctuation">:</span>         <span class="token number">3</span>px <span class="token number">3</span>px <span class="token number">5</span>px <span class="token number">6</span>px <span class="token hexcode">#ccc</span><span class="token punctuation">;</span>  <span class="token comment" spellcheck="true">/* Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */</span>
<span class="token punctuation">}</span></code></pre>
<p>Thats:</p>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token property">box-shadow</span><span class="token punctuation">:</span> [horizontal offset] [vertical offset] [blur radius] [optional spread radius] [color]<span class="token punctuation">;</span></code></pre>
<ol>
<li><strong>The horizontal offset</strong> (required) of the shadow, positive means the shadow will be on the right of the box, a negative offset will put the shadow on the left of the box.</li>
<li><strong>The vertical offset</strong> (required) of the shadow, a negative one means the box-shadow will be above the box, a positive one means the shadow will be below the box.</li>
<li><strong>The blur radius</strong> (required), if set to 0 the shadow will be sharp, the higher the number, the more blurred it will be, and the further out the shadow will extend. For instance a shadow with 5px of horizontal offset that also has a 5px blur radius will be 10px of total shadow.</li>
<li><strong>The spread radius</strong> (optional), positive values increase the size of the shadow, negative values decrease the size. Default is 0 (the shadow is same size as blur).</li>
<li><strong>Color</strong> (required) - takes any color value, like hex, named, <a href="http://css-tricks.com/rgba-browser-support/">rgba</a> or <a href="http://css-tricks.com/yay-for-hsla/">hsla</a>. If the color value is omitted, box shadows are drawn in the foreground color (text <code>color</code>). But be aware, older WebKit browsers (pre Chrome 20 and Safari 6) ignore the rule when color is omitted.</li>
</ol>
<p>Using a semi-transparent color like <code>rgba(0, 0, 0, 0.4)</code> is most common, and a nice effect, as it doesn't completely/opaquely cover what it's over, but allows what's underneath to show through a bit, like a real shadow.</p>
<h4 id="article-header-id-0" class="has-header-link"><a class="article-headline-link" href="#article-header-id-0">#</a>Example</h4>
<div id="example"></div>
<h3 id="article-header-id-1" class="has-header-link"><a class="article-headline-link" href="#article-header-id-1">#</a>Inner Shadow</h3>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token selector"><span class="token class">.shadow</span> </span><span class="token punctuation">{</span>
   <span class="token property">-moz-box-shadow</span><span class="token punctuation">:</span>    inset <span class="token number">0</span> <span class="token number">0</span> <span class="token number">10</span>px <span class="token hexcode">#000000</span><span class="token punctuation">;</span>
   <span class="token property">-webkit-box-shadow</span><span class="token punctuation">:</span> inset <span class="token number">0</span> <span class="token number">0</span> <span class="token number">10</span>px <span class="token hexcode">#000000</span><span class="token punctuation">;</span>
   <span class="token property">box-shadow</span><span class="token punctuation">:</span>         inset <span class="token number">0</span> <span class="token number">0</span> <span class="token number">10</span>px <span class="token hexcode">#000000</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span></code></pre>
<h4 id="article-header-id-2" class="has-header-link"><a class="article-headline-link" href="#article-header-id-2">#</a>Example</h4>
<div id="example2"></div>
<h3 id="article-header-id-3" class="has-header-link"><a class="article-headline-link" href="#article-header-id-3">#</a>Internet Explorer (8 and down) Box Shadow</h3>
<p>You need an extra element, but it's do-able with <code>filter</code>.</p>
<pre rel="HTML" class=" language-markup"><code class=" language-markup"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>shadow1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>content<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    Box-shadowed element
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token selector"><span class="token class">.shadow1</span> </span><span class="token punctuation">{</span>
  <span class="token property">margin</span><span class="token punctuation">:</span> <span class="token number">40</span>px<span class="token punctuation">;</span>
  <span class="token property">background-color</span><span class="token punctuation">:</span> <span class="token function">rgb</span><span class="token punctuation">(</span><span class="token number">68</span>,<span class="token number">68</span>,<span class="token number">68</span><span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token comment" spellcheck="true">/* Needed for IEs */</span>

  <span class="token property">-moz-box-shadow</span><span class="token punctuation">:</span> <span class="token number">5</span>px <span class="token number">5</span>px <span class="token number">5</span>px <span class="token function">rgba</span><span class="token punctuation">(</span><span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">0.6</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token property">-webkit-box-shadow</span><span class="token punctuation">:</span> <span class="token number">5</span>px <span class="token number">5</span>px <span class="token number">5</span>px <span class="token function">rgba</span><span class="token punctuation">(</span><span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">0.6</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token property">box-shadow</span><span class="token punctuation">:</span> <span class="token number">5</span>px <span class="token number">5</span>px <span class="token number">5</span>px <span class="token function">rgba</span><span class="token punctuation">(</span><span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">68</span>, <span class="token number">0.6</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token property">filter</span><span class="token punctuation">:</span> <span class="token property">progid</span><span class="token punctuation">:</span>DXImageTransform<span class="token number">.</span>Microsoft<span class="token number">.</span><span class="token function">Blur</span><span class="token punctuation">(</span>PixelRadius=<span class="token number">3</span>,MakeShadow=true,ShadowOpacity=<span class="token number">0.30</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token property">-ms-filter</span><span class="token punctuation">:</span> <span class="token string">"progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)"</span><span class="token punctuation">;</span>
  <span class="token property">zoom</span><span class="token punctuation">:</span> <span class="token number">1</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token selector"><span class="token class">.shadow1</span> <span class="token class">.content</span> </span><span class="token punctuation">{</span>
  <span class="token property">position</span><span class="token punctuation">:</span> relative<span class="token punctuation">;</span> <span class="token comment" spellcheck="true">/* This protects the inner element from being blurred */</span>
  <span class="token property">padding</span><span class="token punctuation">:</span> <span class="token number">100</span>px<span class="token punctuation">;</span>
  <span class="token property">background-color</span><span class="token punctuation">:</span> <span class="token hexcode">#DDD</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span></code></pre>
<h3 id="article-header-id-4" class="has-header-link"><a class="article-headline-link" href="#article-header-id-4">#</a>One-Side Only</h3>
<p>Using a negative spread radius, you can get squeeze in a box shadow and only push it off one edge of a box.</p>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token selector"><span class="token class">.one-edge-shadow</span> </span><span class="token punctuation">{</span>
  <span class="token property">box-shadow</span><span class="token punctuation">:</span> <span class="token number">0</span> <span class="token number">8</span>px <span class="token number">6</span>px -<span class="token number">6</span>px black<span class="token punctuation">;</span>
<span class="token punctuation">}</span></code></pre>
<div id="example3"></div>
<h3 id="article-header-id-5" class="has-header-link"><a class="article-headline-link" href="#article-header-id-5">#</a>Multiple Borders &amp; More</h3>
<p>You can comma separate box-shadow any many times as you like. For instance, this shows two shadows with different positions and different colors on the same element:</p>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token property">box-shadow</span><span class="token punctuation">:</span> 
  inset <span class="token number">5</span>px <span class="token number">5</span>px <span class="token number">10</span>px <span class="token hexcode">#000000</span>, 
  inset -<span class="token number">5</span>px -<span class="token number">5</span>px <span class="token number">10</span>px blue<span class="token punctuation">;</span></code></pre>
<p>The example shows how you can use that to create multiple borders:</p>
<div class="cp_embed_wrapper resizable" style="height: 557px;"><iframe id="cp_embed_bCqia" src="//codepen.io/chriscoyier/embed/bCqia?height=557&amp;theme-id=1&amp;slug-hash=bCqia&amp;default-tab=css%2Cresult&amp;user=chriscoyier&amp;embed-version=2&amp;pen-title=Multiple%20box-shadow%20coolness!" scrolling="no" frameborder="0" height="557" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="Multiple box-shadow coolness!" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<p>By applying the shadows to pseudo elements which you then manipulate, you can get some pretty fancy 3D looking box shadows:</p>
<div class="cp_embed_wrapper resizable" style="height: 662px;"><iframe id="cp_embed_FxGsI" src="//codepen.io/haibnu/embed/FxGsI?height=662&amp;theme-id=1&amp;slug-hash=FxGsI&amp;embed-version=2&amp;default-tab=result&amp;user=haibnu" scrolling="no" frameborder="0" height="662" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 1" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h3 id="article-header-id-6" class="has-header-link"><a class="article-headline-link" href="#article-header-id-6">#</a>Relevant Links</h3>
<ul>
<li><a href="https://developer.mozilla.org/en/CSS/box-shadow">Mozilla Docs</a></li>
</ul>
<h3 id="browser-support" class="has-header-link"><a class="article-headline-link" href="#browser-support">#</a>Browser Support</h3>
<p>See snippet at top of page for specifics on vendor prefix coverage. This is one of those properties where <a href="http://css-tricks.com/do-we-need-box-shadow-prefixes/">dropping the prefixes is pretty reasonable</a> at this point.</p>


    </div>

  </article>