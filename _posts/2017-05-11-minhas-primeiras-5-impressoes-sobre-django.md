---
layout: post
title: "WHAT IS DJANGO AND WHY IS IT SO POPULAR?"
description: Django came into the scene right as the first big wave of Rails hype was ramping up, and so it was immediately positioned as Python's answer to Rails and thus grabbing eyeballs almost from the start.
image: 'https://res.cloudinary.com/dm7h7e8xj/image/upload/v1504809708/django_g7djdj.jpg'
category: 'django'
tags:
- django
twitter_text: WHAT IS DJANGO AND WHY IS IT SO POPULAR?
introduction: Django came into the scene right as the first big wave of Rails hype was ramping up, and so it was immediately positioned as Python's answer to Rails and thus grabbing eyeballs almost from the start.
---
<section class="post-content">
            <p>Django came into the scene right as the first big wave of Rails hype was ramping up, and so it was immediately positioned as Python's answer to Rails and thus grabbing eyeballs almost from the start. Today, one of the most significant advantages of learning Python is the ability it gives you to use Django. What with tech startups being so hot right now, it’s never been easier or more fun to build your own web application. Django just might be your answer. It has been rapidly gaining popularity for its pragmatic design and ease of use.</p>

<p>Django is a high-level Python Web framework encouraging rapid development and pragmatic, clean design. A web application framework is a toolkit of components all web applications need. The goal here is to allow developers to instead of implementing the same solutions over and over again, focus on the parts of their application that are new and unique to their project. In fact, Django is much more fully featured than many other frameworks out there. It takes care of a lot of the hassle of Web development, letting you focus on writing your application without any need to reinvent the wheel. It's free and open source. Additionally, the Django framework enables you to model your domain and code classes, and before you know it, you already have an ORM. Let’s take a closer look to understand its acclaim better.</p>

<h2 id="itistimetested">It is time-tested</h2>

<p>Often, you’ll notice that Django is one of the first frameworks to respond to a new vulnerability. It’s core team usually alerts other frameworks of patches they should make. There’s a lot to be said about the stability of Django. While nobody claims that every little bug has been fixed, a lot of them have. Today, a lot of the Django releases are focused on edge case concerns and new features. Perhaps software doesn’t get wiser with age, but it often makes better decisions.</p>

<h2 id="youhaveaccesstoenoughdjangopackages">You have access to enough Django packages</h2>

<p>The Django community, like the Python community, contributes a whole lot of useful packages and utilities for use by the wider world. Type in “Django” on PyPI, and you’ll come across over 4,000 packages available for use, and this is on top of Django’s “batteries included” mentality. The framework will most likely house just about everything you’re going to want.</p>

<h2 id="itsbeencrowdtested">It’s been crowd-tested</h2>

<p>Python and Django tend to be a bit quieter, as compared to Rails and Node, both of which receive a lot of publicity from their big users. Of course, this doesn’t mean some major names don't use Django. Django powers many of the Web’s most-used sites, like Instagram and Pinterest, even Facebook uses Django for its many behind-the-scenes utilities. Django came from publishing, so it’s no surprise that sites like The Washington Post and Smithsonian Magazine use Django.</p>

<p><img src="/insights/content/images/2017/10/What-is-Django-1.png" alt=""></p>

<h2 id="djangohaswonderfuldocumentation">Django has wonderful documentation</h2>

<p>Django walked into the world with documentation far above the usual standard for open-source projects, and it has only gotten better over time. When it initially came out, the excellent documentation was one of the features that set Django apart. Most other frameworks at that time merely used an alphabetical list of modules and all the methods and attributes. This works well for quick reference but doesn’t help, when you’re first finding your foot in the framework. Django’s documentation quality isn’t unique anymore, but it’s still definitely one of the best examples of open source documentation in the wild. And maintaining the quality of docs is always a concern for Django’s developers. You see, docs are a first-class citizen in the Django world.</p>

<h2 id="thedjangocommunityishugelysupportive">The Django community is hugely supportive</h2>

<p>It’s often said that the community is one of the best aspects of the Python world, this is even more true for the Django world. Django is governed by the Django Software Foundation or DSF. Every event involving Django has a code of conduct. IN fact, the DSF has released statements on diversity, taking an official stance on the kind of community they envision. In many communities out there, places like IRC and mailing lists are unwelcoming and sometimes, toxic. Here, you’ll find them very pleasant. Of course, there is the occasional rotten apple, but they’re handled quickly. And thanks to these policies, a lot of groups like Django Girls flourish. </p>

<h2 id="djangoadvocatesbestpracticesforseo">Django advocates best practices for SEO</h2>

<p>Web developers and SEOs don’t always play nice with each other. The job of a developer and optimization of the Search Engine, often seem to be at cross-purposes. With Django, you should find this less of an issue. If for nothing else, Python’s Django framework advocates the use of human-readable website URLs, which helps with search engines and isn’t only helpful from the actual user’s perspective, but using the keywords in the URL when ranking sites. Your SEO team will be incredibly grateful. Besides, it makes sense to ensure URLs aren’t just a series of random numbers and letters, that they mean something instead.</p>

<h2 id="scalability">Scalability</h2>

<p>Django is of course, great for getting started and surprisingly enough it's great when it comes to scaling, too. Django, at its heart, is a series of components, wired up and ready-to-go by default. Now, since these components are decoupled, that is not dependent on each other, they can be unplugged and replaced as and when your startup requires more specific solutions.</p>

<h2 id="security">Security</h2>

<p>Django, by default, prevents a whole lot of common security mistakes better than say, PHP does. To begin with, Django camouflages or hides your site’s source code by dynamically generating web pages and through templates sending information to web browsers, from direct viewing on the Internet.</p>

<blockquote>
  <p><em>Finally, there's something to be said for the way Django has been marketed. Or rather, the lack of marketing for a long time. At least not in a way, say, Rails was marketed. Until a while back, the marketing effort mostly consisted of talks at PyCon, people blogging, and then mostly just working on the framework, building amazing things with it and letting the results market themselves. Now, of course, there is the DjangoCon and DSF and business-oriented consultants doing training sessions and many books and all the rest, but this all still quite new. End of the day, the factors listed above are some of the major ones behind the steady rise in popularity Django has seen, since its release.</em></p>
</blockquote>
        </section>