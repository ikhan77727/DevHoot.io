---
layout: post
title: "Everything You Need To Know About CSS3 Box Shadow"
description: "A block level element is any element that has structure and layout. The most common block level element is a DIV, although almost any element can be made a block using display:block"
image: 'https://res.cloudinary.com/dm7h7e8xj/image/upload/c_scale,w_760/v1504807239/morpheus_xdzgg1.jpg'
category: 'blog'
twitter_text: Everything You Need To Know About CSS3 Box Shadow
introduction: A block level element is any element that has structure and layout. The most common block level element is a DIV, although almost any element can be made a block using display:block
---
<div class="post-body"><div class="entry jpibfi_container"> <input class="jpibfi" type="hidden"><p>In previous articles, we have looked in to <a href="https://www.crazyegg.com/blog/css3-text-shadow-effects/" target="_blank">text shadows</a> and <a href="https://www.crazyegg.com/blog/css3-rounded-corners/" target="_blank">border effects</a> for our block level elements – but now it is time to see what box shadow effects we can create on our block level elements with CSS3.</p><p>A block level element is any element that has structure and layout. The most common block level element is a DIV, although almost any element can be made a block using <code>display:block</code>.</p><div id="c3456_1_na" class="sam-pro-container"><div class="section-callout"> <a href="https://www.crazyegg.com?utm_source=Blog&amp;utm_medium=Ad&amp;utm_campaign=QualifiedVisitors_2018-04" target="_blank"><h3 class="section-title">Increase revenue. Improve conversion rates. In 30-days.</h3><p>Enjoy your FREE 30-day trial, starting today</p><span class="btn btn-blue-bg">Show Me My Heatmap <i class="ico-arrow-right"></i></span><img src="https://www.crazyegg.com/blog/wp-content/plugins/crazyegg-topbar/images/logo_white.svg" data-lazy-type="image" data-src="https://www.crazyegg.com/blog/wp-content/plugins/crazyegg-topbar/images/logo_white.svg" alt="" style="height: 50px; width: 96px;" class="no-lightbox lazy-loaded" data-jpibfi-indexer="0"><noscript><img src="https://www.crazyegg.com/blog/wp-content/plugins/crazyegg-topbar/images/logo_white.svg" alt="" style="height: 50px; width: 96px;" class="no-lightbox"></noscript> </a></div></div><p>The CSS property <code>box-shadow</code> gives us a huge variety of options for creating shadow effects both inside and outside the element.</p><p>The syntax is as follows (all on one line):</p><pre>box-shadow:
inset xPosition yPosition blurSize spreadSize color;</pre><p>Where:</p><ul><li><strong>inset</strong> (optional) if included the box shadow exists inside the box. If omitted, the box shadow exists outside the box.</li><li><strong>xPosition</strong> is the horizontal offset of the box shadow relative to the block level element.</li><li><strong>yPosition</strong> is the vertical offset of the box shadow relative to the block level element.</li><li><strong>blurSize</strong> (optional) is the size of the shadows’ blur.</li><li><strong>spreadSize</strong> (optional) is the size of the shadows’ spread.</li><li><strong>color</strong> is the color value – and can be any of the usual color formats – hex, rgb, rgba, hsl, hsla or a named color.</li></ul><p>Assuming we had a DIV on our page with the class myElement, we could create a simple box shadow on that element using the code:</p><pre>.myElement {
    box-shadow:2px 2px 2px #666;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_01.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_01.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_01.gif" data-jpibfi-indexer="1"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_01.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_01.gif" ></noscript></p><p>In the code snippet above, we are setting the x and y positions of the shadow to 2 pixels to create a shadow to the bottom right. We also set the blur size to 2 pixels and the color to #666 (grey).</p><p>If we wished the shadow to go to the top left, rather than the bottom right we simply use negative values for the x and y shadow position.</p><pre>.myElement {
    box-shadow:-2px -2px 2px #666;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_02.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_02.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_02.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_02.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_02.gif" ></noscript></p><p>Using the xPosition and yPosition values without a blur, we can create a block shadow with an offset, like so:</p><pre>.myElement {
    box-shadow:2px 2px 0 #666;
}</pre><p><img class="alignnone size-full wp-image-4984 lazy-loaded" style="margin-left: -19px;" alt="" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_03.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_03.gif" width="227" height="61" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_03.gif"><noscript><img class="alignnone size-full wp-image-4984" style="margin-left: -19px;" alt="" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_03.gif" width="227" height="61" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_03.gif" ></noscript></p><p>If you want to create a box shadow all around the element, you don’t need to create four box shadows. Instead, use spreadSize to create a block shadow around the element, like so:</p><pre>.myElement {
    box-shadow:0 0 0 2px #666;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_04.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_04.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_04.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_04.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_04.gif" ></noscript></p><p>Want your block shadow to be blurred? No problem!</p><pre>.myElement {
    box-shadow:0 0 5px 2px #666;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_05.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_05.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_05.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_05.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_05.gif" ></noscript></p><p>If you wish your shadow to appear inside your element, use the keyword inset. Positive numbers will make the shadow appear to the left (on the xPosition) and above (on the yPosition).</p><pre>.myElement {
    box-shadow:inset 2px 2px 2px #000;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_06.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_06.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_06.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_06.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_06.gif" ></noscript></p><p>While negative numbers will make the shadow appear on the right and bottom edge.</p><pre>.myElement {
    box-shadow:inset -2px -2px 2px #000;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_07.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_07.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_07.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_07.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_07.gif" ></noscript></p><p>Let’s do something useful with inset. Ever wanted to create a button with a glow effect? With box shadow, it’s easy! The following code fills half the button with a different color, with a simple blur at the middle.</p><pre>.myElement {
    box-shadow:inset 0 25px 2px #92baf4;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_08.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_08.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_08.gif" data-jpibfi-indexer="6"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_08.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_08.gif" ></noscript></p><p>Again, you can fill your shadow from the bottom to create an effect in the opposite direction.</p><pre>.myElement {
    box-shadow:inset 0 -25px 2px #92baf4;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_09.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_09.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_09.gif" data-jpibfi-indexer="5"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_09.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_09.gif" ></noscript></p><p>Like text shadow, you are not restricted to just one shadow. Using a comma, you can create multiple shadows. Here, we’ve created a shadow both inside and outside the box.</p><pre>.myElement {
    box-shadow:2px 2px 2px #666, inset 0 -25px 2px #92baf4;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_10.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_10.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_10.gif"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_10.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_10.gif" ></noscript></p><p>Using border-radius from the last blog, we can combine the two to create more rounded button. This looks nicer, but the shadow will take on the same radius as the box</p><pre>.myElement {
    border-radius:8px;
    box-shadow:2px 2px 2px #666, inset 0 25px 0 #92baf4;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_11.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_11.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_11.gif" data-jpibfi-indexer="4"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_11.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_11.gif" ></noscript></p><p>Fortunately, there is a way to solve this. By adding a negative spread, we can reduce the border-radius effect. You may need to play with the negative value to get the setting right for you.</p><pre>.myElement {
    border-radius:8px;
    box-shadow:2px 2px 2px #666, inset 0 60px 0 -36px #92baf4;
}</pre><p><img class="alignnone size-full lazy-loaded" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_12.gif" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_12.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_12.gif" data-jpibfi-indexer="3"><noscript><img class="alignnone size-full" alt="box shadow example" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_12.gif" width="273" height="58" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/box-shadow_12.gif" ></noscript></p><p>So which browsers support box-shadow? Thankfully more than those that support border-image!</p><p><img class="alignnone size-full wp-image-5102 lazy-loaded" style="margin-left: 0;" alt="Browser Matrix" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg" data-lazy-type="image" data-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg" width="480" height="126" srcset="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg 480w, https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers-300x78.jpg 300w" data-srcset="" sizes="(max-width: 480px) 100vw, 480px" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg" data-jpibfi-indexer="2"><noscript><img class="alignnone size-full wp-image-5102" style="margin-left: 0;" alt="Browser Matrix" src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg" width="480" height="126" srcset="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg 480w, https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers-300x78.jpg 300w" sizes="(max-width: 480px) 100vw, 480px" data-jpibfi-post-excerpt="This article takes a look at the box shadow effects we can create on our block level elements with CSS3." data-jpibfi-post-url="https://www.crazyegg.com/blog/css3-box-shadow/" data-jpibfi-post-title="Everything You Need To Know About CSS3 Box Shadow" data-jpibfi-src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2012/05/browsers.jpg" ></noscript></p><p>All green for all browsers – well, IE9 and beyond. It’s also supported on mobile, but if you want to use it on Android and iOS 3 and 4, you’ll need to add the -webkit- prefix. Similarly if you are still supporting Firefox 3.6, you’ll want to add the -moz- prefix.</p><p>If you’ve created any awesome box shadow effects, let us know in the comments!</p></div><!-- /.entry --></div>