---
layout: post
title: Use Cases for Fixed Backgrounds in CSS
date: 2017-08-14 22:55:45
image: >-
  https://res.cloudinary.com/dm7h7e8xj/image/upload/c_fill,h_399,w_760/v1503153729/golpe_ghb84o.jpg
description: 'I have always looked at it as some sort of old school design trick from the GeoCities days to get a repeating background to stay in place during scroll.'
category: life
tags:
  - life
  - tips
twitter_text: 'Use Cases for Fixed Backgrounds in CSS'
introduction: >-
 I have always looked at it as some sort of old school design trick from the GeoCities days to get a repeating background to stay in place during scroll.
published: true
---
<div class="article-content">

      
      <p>File this into a category of personal "<a href="https://css-tricks.com/moment-css-started-making-sense/">CSS Ah-Ha Moments</a>".</p>
<p>The <code>background-attachment</code> property has never seemed all that useful to me. I have always looked at it as some sort of old school design trick from the GeoCities days to get a repeating background to stay in place during scroll.</p>
<p><span id="more-244748"></span></p>
<div class="cp_embed_wrapper resizable" style="height: 266px;"><iframe id="cp_embed_QEPQqp" src="//codepen.io/geoffgraham/embed/QEPQqp?height=266&amp;theme-id=279&amp;slug-hash=QEPQqp&amp;default-tab=result&amp;user=geoffgraham&amp;embed-version=2" scrolling="no" frameborder="0" height="266" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 9" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<p>Turns out a background with a fixed position can be much more useful than that. Adding the single line <code>background-attachment: fixed;</code> to an element can actually give us some power for making smooth, graceful transitions between content in a way that adds to the user experience without any other dependencies, like Javascript or intense animations.</p>
<h3 id="article-header-id-0" class="has-header-link"><a class="article-headline-link" href="#article-header-id-0">#</a>The Faux Slide Deck</h3>
<p>Making a presentation? A single page broken up into "slides" is pretty straightforward:</p>
<pre rel="CSS" class=" language-css"><code class=" language-css"><span class="token selector"><span class="token class">.slide</span> </span><span class="token punctuation">{</span>
  <span class="token property">background-image</span><span class="token punctuation">:</span> <span class="token url">url('path-to-url')</span><span class="token punctuation">;</span>
  <span class="token property">background-attachment</span><span class="token punctuation">:</span> fixed<span class="token punctuation">;</span>
  <span class="token property">height</span><span class="token punctuation">:</span> <span class="token number">100</span>vh<span class="token punctuation">;</span>
  <span class="token property">width</span><span class="token punctuation">:</span> <span class="token number">100%</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span></code></pre>
<p>When we call the <code>.slide</code> element three times in our HTML, then we have slides that appear to overlap one another as the page scrolls down.</p>
<div class="cp_embed_wrapper resizable" style="height: 300px;"><iframe id="cp_embed_NAEXyg" src="//codepen.io/geoffgraham/embed/NAEXyg?height=300&amp;theme-id=279&amp;slug-hash=NAEXyg&amp;default-tab=result&amp;user=geoffgraham&amp;embed-version=2" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 8" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<p>No libraries. No scroll-jacking. Pure CSS with full browser support (well, minus touch screens). Might be a fun thing to pair with <a href="https://css-tricks.com/introducing-css-scroll-snap-points/">CSS Scroll Snap Points</a>.</p>
<h3 id="article-header-id-1" class="has-header-link"><a class="article-headline-link" href="#article-header-id-1">#</a>The "Slide Over The Header" Header</h3>
<p>Let's say we wanted some fancy header that gets overlapped by content. We can do that as well.</p>
<div class="cp_embed_wrapper resizable" style="height: 365px;"><iframe id="cp_embed_akrJjY" src="//codepen.io/geoffgraham/embed/akrJjY?height=365&amp;theme-id=279&amp;slug-hash=akrJjY&amp;default-tab=result&amp;user=geoffgraham&amp;embed-version=2" scrolling="no" frameborder="0" height="365" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 7" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h3 id="article-header-id-2" class="has-header-link"><a class="article-headline-link" href="#article-header-id-2">#</a>The Faux Transparency</h3>
<p>Remember when classrooms had <a href="https://en.wikipedia.org/wiki/Overhead_projector">overhead projectors</a> and teachers would have to create what they called transparencies to present layered information? We can do the same!</p>
<div class="cp_embed_wrapper resizable" style="height: 300px;"><iframe id="cp_embed_pbQakX" src="//codepen.io/geoffgraham/embed/pbQakX?height=300&amp;theme-id=279&amp;slug-hash=pbQakX&amp;default-tab=result&amp;user=geoffgraham&amp;embed-version=2" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 6" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h3 id="article-header-id-3" class="has-header-link"><a class="article-headline-link" href="#article-header-id-3">#</a>Other Awesome Examples</h3>
<p>There are so many other great examples of this in action over on CodePen. Here's are a few to feast on.</p>
<h4 id="article-header-id-4" class="has-header-link"><a class="article-headline-link" href="#article-header-id-4">#</a>Sliding Panels At The Beginning (But Also In The Middle) Of An Article</h4>
<div class="cp_embed_wrapper resizable" style="height: 300px;"><iframe id="cp_embed_RRJWAA" src="//codepen.io/szentz/embed/RRJWAA?height=300&amp;theme-id=279&amp;slug-hash=RRJWAA&amp;default-tab=result&amp;user=szentz&amp;embed-version=2" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 5" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h4 id="article-header-id-5" class="has-header-link"><a class="article-headline-link" href="#article-header-id-5">#</a>Angled And Transparent Overlapping Sections</h4>
<div class="cp_embed_wrapper resizable" style="height: 300px;"><iframe id="cp_embed_XKpzqb" src="//codepen.io/carpenumidium/embed/XKpzqb?height=300&amp;theme-id=279&amp;slug-hash=XKpzqb&amp;default-tab=result&amp;user=carpenumidium&amp;embed-version=2" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 4" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h4 id="article-header-id-6" class="has-header-link"><a class="article-headline-link" href="#article-header-id-6">#</a>Scrolling Flip Book</h4>
<div class="cp_embed_wrapper resizable" style="height: 423px;"><iframe id="cp_embed_mHFkd" src="//codepen.io/derekjp/embed/mHFkd?height=423&amp;theme-id=279&amp;slug-hash=mHFkd&amp;default-tab=result&amp;user=derekjp&amp;embed-version=2" scrolling="no" frameborder="0" height="423" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 3" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h4 id="article-header-id-7" class="has-header-link"><a class="article-headline-link" href="#article-header-id-7">#</a>Mid Article Background Header Breaks</h4>
<div class="cp_embed_wrapper resizable" style="height: 547px;"><iframe id="cp_embed_vKOvgJ" src="//codepen.io/samstevenson/embed/vKOvgJ?height=547&amp;theme-id=1&amp;slug-hash=vKOvgJ&amp;default-tab=result&amp;user=samstevenson&amp;embed-version=2" scrolling="no" frameborder="0" height="547" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 2" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
<h4 id="article-header-id-8" class="has-header-link"><a class="article-headline-link" href="#article-header-id-8">#</a>Clipped Headers Within Fixed Panels</h4>
<p>This one doesn't actually use background-attachment, but it's very cool and a related effect. </p>
<div class="cp_embed_wrapper resizable" style="height: 534px;"><iframe id="cp_embed_WxoVBo" src="//codepen.io/StephenScaff/embed/WxoVBo?height=534&amp;theme-id=1&amp;slug-hash=WxoVBo&amp;default-tab=result&amp;user=StephenScaff&amp;embed-version=2" scrolling="no" frameborder="0" height="534" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" name="CodePen Embed" title="CodePen Embed 1" class="cp_embed_iframe " style="width: 100%; overflow: hidden; height: 100%;"></iframe><div class="win-size-grip" style="touch-action: none;"></div></div>
