---
layout: page
title: About
description: Some description.
permalink: /about/
---

<img itemprop="image" class="img-rounded" src="https://i.imgur.com/BUZF6kml.jpg" alt="Your Name">

## About

DevHoot is an online publication that showcases web & software development articles, tutorials, and news, from writers all over the world.
